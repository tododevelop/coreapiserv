﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreApiServ.Model;
using FileRequest.Core;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CoreApiServ.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class GetFilesController : ControllerBase
    {
        public GetFilesController(IWebHostEnvironment appEnvironment)
        {
            this.appEnvironment = appEnvironment;

        }
        private readonly IWebHostEnvironment appEnvironment;

        [HttpPost]
        public RequestState Get()
        {
            var logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
            logger.Trace($"Мы в контроллере!");
            string[] urls = new string[1];
            try
            {
                var contentType = Request.ContentType;

                logger.Trace($"ContentType = '{contentType}'");

                if (Request.Form == null)
                {
                    logger.Trace($"Request.Form = null");
                }
                else
                {
                    logger.Trace($"Request.Form != null. Request.Form.Count() = {Request.Form.Count()}");
                }
                if (Request.Form?.Keys?.Where(p => p.Substring(0, 4) == "url[").Count() > 0)
                {
                    var a = Request.Form.Keys.Where(p => p.Substring(0, 4) == "url[").ToList();
                    logger.Trace($"Количество urls: {a.Count}");
                    urls = new string[a.Count()];
                    var i = 0;
                    foreach (var k in Request.Form?.Where(p => p.Key.Substring(0, 4) == "url["))
                    {
                        urls[i] = k.Value;
                        logger.Trace($"Получили параметры url. Его значение: {k.Value}");
                        i++;
                    }
                }
                else
                {
                    logger.Error($"Не заданны параметры url.");
                }

            }
            catch (Exception e)
            {
                Exception ex = e;
                string errMess = "";
                while (ex != null)
                {
                    errMess += "\n" + ex.Message;
                    ex = ex.InnerException;
                }
                logger.Error(e, $"Ошибка при запросе параметра url! {errMess}");
                return new RequestState { ErrorMessage = errMess, IsOk = false, Result = null };
            }

            var retVal = new RequestState();
            var domain = new FileDomain(appEnvironment);

            logger.Trace($"Идем в GetFiles(url)");

            if (urls != null)
            {
                var fl = domain.GetFiles(urls);
                retVal.IsOk = true;
                logger.Trace($"JsonConvert.SerializeObject(fl)");
                //retVal.Result = JsonConvert.SerializeObject(fl);
                retVal.Result = fl;
                logger.Trace($"Return");
                return retVal;
            }
            retVal.IsOk = false;
            logger.Trace($"urls == null");
            retVal.Result = null;
            logger.Trace($"Return");
            return retVal;
        }
    }
}