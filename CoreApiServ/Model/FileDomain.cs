﻿using FileRequest.Core;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace CoreApiServ.Model
{
    /// <summary>
    /// Логика работы с файлами
    /// </summary>
    public class FileDomain
    {
        public FileDomain()
        {

        }
        public FileDomain(IWebHostEnvironment appEnvironment)
        {
            this.appEnvironment = appEnvironment;
        }

        private readonly IWebHostEnvironment appEnvironment;

        /// <summary>
        /// Загрузка файлов из интернета по заданным url
        /// </summary>
        /// <param name="urls">Массив url</param>
        /// <returns>Коллекция транспортных объектов полученных из интернета файлов</returns>
        internal List<FileDto> GetFiles(string[] urls)
        {
            var result = new List<FileDto>();
            int i = 0;
            foreach (var url in urls)
            {
                var fileDto = GetFile(url, i);
                result.Add(fileDto);
                i++;
            }
            return result;
        }

        /// <summary>
        /// Загрузка файлов из интернета по заданным url
        /// </summary>
        /// <param name="urls">Массив url</param>
        /// <returns>Коллекция транспортных объектов полученных из интернета файлов</returns>
        internal List<FileUrlDto> GetFilesWithUrls(string[] urls, string host)
        {
            var result = new List<FileUrlDto>();
            int i = 0;
            foreach (var url in urls)
            {
                var fileDto = GetFileWithUrl(url, i, host);
                result.Add(fileDto);
                i++;
            }
            return result;
        }

        /// <summary>
        /// Получить файл по url
        /// </summary>
        /// <param name="url">url</param>
        /// <returns>Тело файла</returns>
        private FileUrlDto GetFile(string url,int i)
        {
            var logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
            try
            {
                var wc = new WebClient();
                var b = wc.DownloadData(url);
                var contentType = wc.ResponseHeaders["Content-Type"];
                //var duration = GetDuration(b, contentType);
                return new FileUrlDto { Body = (b != null)? b: new byte[0], Name = (b != null) ? "file" +i.ToString() : "Error Loading File", Size = (b != null) ? b.Length : 0, MimeType = contentType };
            }
            catch (Exception e)
            {
                Exception ex = e;
                string errMess = "";
                while (ex != null)
                {
                    errMess += "\n" + ex.Message;
                    ex = ex.InnerException;
                }
                logger.Error(e, $"Ошибка при загрузке файла по ссылке: {url}{errMess}");
                return new FileUrlDto { Body =  new byte[0], Name = $"Ошибка при загрузке файла по ссылке: {url}{errMess}", Size =  0, MimeType = "" };
            }
        }

        private FileUrlDto GetFileWithUrl(string url, int i, string host)
        {
            var file = GetFile(url, i);
            if (file.Size > 0)
            {
                // путь к папке Files
                var ext = file.MimeType switch
                {
                    var x when (x.Contains("mp4")) => ".mp4",
                    var x when (x.Contains("jpeg")) => ".jpg",
                    var x when (x.Contains("text/html")) => ".html",
                    var x when (x.Contains("text/plain")) => ".txt",
                    var x when (x.Contains("image/tiff")) => ".tiff",
                    var x when (x.Contains("gif")) => ".gif",
                    var x when (x.Contains("png")) => ".png",
                    var x when (x.Contains("mpeg")) => ".mp3",
                    _ => ""
                };

                file.Name += Guid.NewGuid().ToString().ToLower() + ext;
                string path = "/Files/" + file.Name;
                // сохраняем файл в папку Files в каталоге wwwroot

                using (var stream = new FileStream(appEnvironment.WebRootPath + path, FileMode.Create))
                {
                    stream.Write(file.Body, 0, file.Body.Length);
                }
                file.ServerUrl = host + @"/Files/" + file.Name;
            }
            return file;
        }
        //private TimeSpan GetDuration(byte[] body, string mimetype)
        //{
        //    if (mimetype.Contains("mp4"))
        //    {
        //        var fileName = Guid.NewGuid().ToString().ToUpper() + ".MP4";
        //        string path = @"\Files\" + fileName;
        //        using (var stream = new FileStream(appEnvironment.WebRootPath + path, FileMode.Create))
        //        {
        //            stream.Write(body, 0, body.Length);
        //        }
        //        //var r = FFmpeg.GetMediaInfo(path);
        //        //r.Wait();
        //        //var minfo = r.Result;
        //        //var videoDuration = minfo.Duration;
        //    }
        //    return new TimeSpan();
        //}
    }
}
